#+TITLE: GitLab CI
#+AUTHOR: Inria BSO-SED
#+LANGUAGE:  en
#+OPTIONS: H:3 num:t \n:nil @:t ::t |:t _:nil ^:nil -:t f:t *:t <:t
#+OPTIONS: TeX:t LaTeX:t skip:nil d:nil pri:nil tags:not-in-toc html-style:nil
#+BEAMER_THEME: Rochester
#+HTML_HEAD:   <link rel="stylesheet" title="Standard" href="css/worg.css" type="text/css" />
#+HTML_HEAD:   <link rel="stylesheet" type="text/css" href="css/VisuGen.css" />
#+HTML_HEAD:   <link rel="stylesheet" type="text/css" href="css/VisuRubriqueEncadre.css" />

This presentation has been moved here:
https://gitlab.inria.fr/sed-bso/gitlab-ci
